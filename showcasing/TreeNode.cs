﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showcasing
{
    class TreeNode
    {
        
        public TreeNode(int v)
        {
            value = v;
        }

        public int value { get; set; }
        public TreeNode LeftTreeNode { get; set; }
        public TreeNode RightTreeNode { get; set; }

        internal void add(TreeNode newNode)
        {
            if (newNode.value > value && RightTreeNode == null)
            {
                RightTreeNode = newNode;
            }
            else if (newNode.value > value) 
            {
                RightTreeNode.add(newNode);
            }
            else if (newNode.value < value && LeftTreeNode== null)
            {
                LeftTreeNode = newNode;
            }
            else if (newNode.value < value) 
            {
                LeftTreeNode.add(newNode);
            }
            else
            {
                Console.WriteLine("the node you are trying to add already equals a node that we have... so we aren't adding it");
            } 
            


        }

        internal void traverse()
        {
            if (LeftTreeNode != null)
            {
                LeftTreeNode.traverse();
            }

            Console.WriteLine(value + ", ");

            if (RightTreeNode != null)
            {
                RightTreeNode.traverse();
            }
        }
    }
}
