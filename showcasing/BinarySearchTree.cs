﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace showcasing
{
    class BinarySearchTree
    {

        public TreeNode Root { get; set; }

        public BinarySearchTree(TreeNode root)
        {
            Root = root;
        }




        public void add (TreeNode newNode)
        {
            Root.add(newNode);
        }


        public void traverseInOrder()
        {
            Root.traverse();
        }




    }
}
